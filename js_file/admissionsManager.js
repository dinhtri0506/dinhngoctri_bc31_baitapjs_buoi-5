var regional_A = "regional_a";
var regional_B = "regional_b";
var regional_C = "regional_c";
// Xác định điểm cộng khi thí sinh thuộc khu vực a, b, c hoặc khu vực không ưu tiên
var takeRegionalBonusPoints = function (regional) {
  switch (regional) {
    case regional_A:
      return 2;
    case regional_B:
      return 1;
    case regional_C:
      return 0.5;
    default:
      return 0;
  }
};

var object_1 = "object_1";
var object_2 = "object_2";
var object_3 = "object_3";
// Xác định điểm cộng khi thí sinh thuộc đối tượng a, b, c hoặc đối tượng không ưu tiên
var takeObjectBonusPoints = function (object) {
  switch (object) {
    case object_1:
      return 2.5;
    case object_2:
      return 1.5;
    case object_3:
      return 1;
    default:
      return 0;
  }
};

document
  .getElementById("btn-result")
  .addEventListener("click", admissionsManager);

function admissionsManager() {
  // Lấy giá trị khu vực ưu tiên khi thí sinh check vào (*)
  var regionalValue = document.querySelector(
    'input[name="regional-bonus-points"]:checked'
  ).value;
  // Trả về điểm cộng, điểmCộngKhuVực(giá trị khu vực) (**)
  var regionalBonusPoints = takeRegionalBonusPoints(regionalValue);
  // Tương tự (*)
  var objectValue = document.querySelector(
    'input[name="object-bonus-points"]:checked'
  ).value;
  // Tương tự (**)
  var objectBonusPoints = takeObjectBonusPoints(objectValue);
  // Tổng điểm cộng của thí sinh
  var totalBonusPoint = regionalBonusPoints + objectBonusPoints;

  /* ------------------------------------------------------------ */

  // Lấy lần lượt giá trị điểm toán, lý, hoá
  var mathScores = document.getElementById("math-scores").value * 1;
  var physicScores = document.getElementById("physic-scores").value * 1;
  var chemistryScores = document.getElementById("chemistry-scores").value * 1;
  // Tổng điểm thi ba môn
  var totalScoresOfThreeSubjects = mathScores + physicScores + chemistryScores;

  /* ------------------------------------------------------------ */

  // Lấy giá trị điểm xét tuyển
  var entryBenchmark = document.getElementById("entry-benchmark").value * 1;
  // Điểm tổng kết
  var finalScores = totalBonusPoint + totalScoresOfThreeSubjects;
  // Kiểm tra kết quả đậu hay trượt
  if (
    mathScores >= 0 &&
    physicScores >= 0 &&
    chemistryScores >= 0 &&
    entryBenchmark >= 0
  ) {
    if (
      mathScores > 0 &&
      physicScores > 0 &&
      chemistryScores > 0 &&
      finalScores >= entryBenchmark
    ) {
      document.getElementById(
        "show-result-div"
      ).innerHTML = `<div class="m-auto">
        <h3>Tổng điểm cộng: ${totalBonusPoint}</h3>
        <h3>Tổng điểm ba môn: ${totalScoresOfThreeSubjects}</h3>
        <h3 class="display-4 text-center text-primary mt-4">${finalScores}</h3>
        <h3 class="display-4 text-center text-primary">Bạn đã đậu</h3>
        </div>`;
      console.log("pass");
    } else {
      document.getElementById(
        "show-result-div"
      ).innerHTML = `<div class="m-auto">
        <h3>Tổng điểm cộng: ${totalBonusPoint}</h3>
        <h3>Tổng điểm ba môn: ${totalScoresOfThreeSubjects}</h3>
        <h3 class="display-4 text-center text-danger mt-4">${finalScores}</h3>
        <h3 class="display-4 text-center text-danger">Bạn đã trượt</h3>
        </div>`;
      console.log("fail");
    }
  } else {
    alert(
      "Giá trị nhập vào của điểm Toán, Lý, Hoá và điểm xét tuyển phải lớn hơn hoặc bằng 0."
    );
  }

  console.log({ regionalBonusPoints, objectBonusPoints });
  console.log({ totalBonusPoint });
  console.log({ mathScores, physicScores, chemistryScores });
  console.log({ totalScoresOfThreeSubjects });
  console.log({ finalScores, entryBenchmark });
}
