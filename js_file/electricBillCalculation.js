document
  .getElementById("btn-show-bill")
  .addEventListener("click", electricBillCalculation);

function electricBillCalculation() {
  // Lấy giá trị đầu vào của tên để in hoá đơn
  var fullNameValue = document.getElementById("full-name-input").value;
  // Lấy giá trị đầu vào của kW đã sử dụng và ép sang kiểu số
  var kWValue = document.getElementById("kW").value * 1;
  // Tạo biến lần lượt cho
  // 50kW đầu tiên
  var first50kW = 50 * 500;
  // 100kW đầu tiên
  var first100kW = first50kW + 50 * 650;
  // ...
  var first200kW = first100kW + 100 * 850;
  var first350kW = first200kW + 150 * 1100;
  var electricBill;

  // Kiểm tra giá trị nhập vào có âm hay không.
  if (kWValue >= 0) {
    // Nếu không, tính trường hợp đầu tiên. Người dùng sử dụng ít hơn 50kW
    if (kWValue <= 50) {
      electricBill = kWValue * 500;
    }
    // Người dùng sử dụng nhiều hơn 50kW và ít hơn 100kW
    else if (kWValue <= 100) {
      electricBill = first50kW + (kWValue - 50) * 650;
    }
    // Tương tự ...
    else if (kWValue <= 200) {
      electricBill = first100kW + (kWValue - 100) * 850;
    } else if (kWValue <= 350) {
      electricBill = first200kW + (kWValue - 200) * 1100;
    } else {
      electricBill = first350kW + (kWValue - 350) * 1300;
    }
    // In hoá đơn
    document.getElementById("show-bill").innerHTML = `<div class="m-auto">
<h4 class="text-center text-primary mb-4">${fullNameValue}</h4>
<h4>Số kW điện đã sử dụng: ${kWValue}</h4>
<h4>Số tiền cần thanh toán:</h4>
<h3 class="display-4 text-center text-primary mt-4">${new Intl.NumberFormat(
      "de-DE",
      {
        style: "currency",
        currency: "VND",
      }
    ).format(electricBill)}</h3>
</div>`;
  }
  // Nếu giá trị nhập vào nhỏ hơn không, hiện thông báo
  else {
    alert("Số kW điện phải lớn hơn hoặc bằng 0");
  }

  console.log(
    new Intl.NumberFormat("de-DE", {
      style: "currency",
      currency: "VND",
    }).format(electricBill)
  );
}
